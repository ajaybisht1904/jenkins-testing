package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TestApplicationTests {
	

	@Test
	void test1() {
		Calculator c=new Calculator();
		int sum = c.add(4, 5);
		assertEquals(9, sum);
	}

	@Test
	void test2() {
		Calculator c=new Calculator();

		int sum = c.add(3, 2);
		assertEquals(5, sum);
	}
	
	
	@Test
	void test3() {
		Calculator c=new Calculator();

		int sum = c.add(4, 8);
		assertEquals(12, sum);
		
	}
	
	

}
