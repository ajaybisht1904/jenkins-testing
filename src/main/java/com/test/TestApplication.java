package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}

	@GetMapping("")
	public String test() {
		return "update success!!";
	}

	@GetMapping("/test-2")
	public String test2() {
		return "test 2 success!!";
	}

	@GetMapping("/master")
	public String master() {
		return "master success!!";
	}
}
